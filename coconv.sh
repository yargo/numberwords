#!/bin/sh
# coordinate encoding/decoding
# using numwords.sh for conversion
# 2015,2017 Y.Bonetti

# conversion script
numwords="sh `dirname $0`/numwords.sh"

# precision of conversion
prec=`$numwords :p`

# number of conversion words per coordinate
wnum=`$numwords :n`

# for debugging
#echod(){ echo ":: $@" >&2 ; }
# non-debug version
echod(){ : ; }

printhelp(){ cat <<EOH

usage: $0 [:osm|:geo] [words|coords|geo-URI]
  will convert between words from wordlists, and coordinates of the form
  /[+-][0-9.]+ [+-][0-9.]+/ where the first value denotes latitude -90 (south)
  to +90 (north) and the second value longitude -180 (west) to +180 (east);
  word conversion is done with $numwords
  using $wnum words for each coordinate
  with precision of conversion at about 10^-$prec;
  input of geo-URIs "geo:LAT,LON" will also be recognized;
  option :osm will generate URLs for openstreetmap.org;
  option :geo will generate generic geo: URI

EOH
}

# convert word list into coordinate pair
tocoords(){
 local wlat wlon i slat slon
 echod convert words "'$@'" to coords
 i=$wnum
 wlat=''
# get wnum words for latitude
 while test $i -gt 0
 do
  wlat="$wlat $1"
  shift
  i=`expr $i - 1`
 done
# get remainder for longitude
 wlon="$*"
 echod split: $wlat,$wlon
# convert words to fractions
 slat=`$numwords $wlat`
 slon=`$numwords $wlon`
# convert fractions to coordinates, convert dc '_' to '-'
 echo "$prec k $slon $slat 180*90-n[ ]n360*180-p" | dc | tr '_' '-'
}


# convert coordinate pair into word list
towords(){
 local lat lon wds cw
 echod $1,$2
# convert normal sign convention into dc convention
 lat=`echo $1|tr '+-' ' _'`
 lon=`echo $2|tr '+-' ' _'`
# lat=(90+latitude)/180, limit between 0 and 1-(10^-prec)/2
 lat=`echo "$prec k $lat 90+180/ 0sM 1 10 _$prec^2/-sP d0>Md1!>Pp" | dc`
# lon=(180+lon)/360, limit as well
 lon=`echo "$prec k $lon 180+360/ 0sM 1 10 _$prec^2/-sP d0>Md1!>Pp" | dc`
 echod normalized: $lat,$lon
 { $numwords $lat ; $numwords $lon ; } | { cw=''
 while read wds
 do cw="$cw $wds"
 done
 echo $cw
 }
} # towords

# generate output format
outfrmt(){
 read lat lon _
 case $1 in
  osm) echo "http://openstreetmap.org/?mlat=$lat&mlon=$lon" ;;
  geo) echo "geo:$lat,$lon" ;;
  *) echo $lat $lon ;;
 esac
} # outfrmt

# convert geo-URI to coordinate pair
geoin(){
 echo $* | sed -e 's/geo://;s/,/ /;s/?.*//'
}

if test "$1" = ""
then printhelp
exit 9
fi

frmt=standard
case $1 in
 :osm) shift ; frmt=osm ;;
 :geo) shift ; frmt=geo ;;
esac

echod format $frmt

case $1 in
 [-+0-9.]*) towords $1 $2 ;;
 geo:*) towords `geoin $*` ;;
 [a-z]*) tocoords $* | outfrmt $frmt ;;
 *) printhelp ;;
esac
