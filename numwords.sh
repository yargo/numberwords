#!/bin/sh
# encoding/decoding of a fractional number with wordlists
# 2015,2017 Y.Bonetti

# wordlist names and containing directory
# (lists must contain total number on first line, and numbered words on
# subsequent lines, numbers going from 0 to total-1)
# default values
cd `dirname $0`
mydir=`pwd -P`
wldir="$mydir/Dict/"
cd - >/dev/null
wlists="$wldir/five $wldir/four"
# use environment value, if present, otherwise defaults
wlists=${NUMBERWORDS:-$wlists}

# name for dynamically generated dc script file
dcsc=${TMPDIR:-/tmp}/dcsc$$.tmp
#dcsc=ddd.tmp # for testing

# for debugging
#echod(){ echo ":: $@" >&2 ; }
# non-debug version
echod(){ : ; }

# create vector with wordlist lengths, also count them
wllen=''
wlcnt=0
for wl in $wlists
do
# for each list, read first line, get second word (number)
 ww=`head -n 1 $wl | { read _ len _ ; echo $len; }`
 wllen="$wllen $ww"
 wlcnt=`expr $wlcnt + 1`
done
echod wllen $wllen "($wlcnt)"

# calculate precision 
echo 1 >$dcsc
echo $wllen | sed -e 's/ /*/g' >>$dcsc
# multiply all wordlist lengths, take number of digits
echo '*Zp' >>$dcsc
dcprec=`dc -f $dcsc`
# for safety reasons: calculate with two more digits
dcprec2=`expr $dcprec + 2`

pickwords(){ # names of wordfiles as arguments, words or index-list in stdin
while read idx _ # _ to absorb possible garbage after indices
do echod "$1($idx:$r)"
 case $idx in
# if integer, get corresponding line from wordlist and related word
 0|[1-9]*) grep "^[ 	]*$idx[	 ]*" $1 |
         { read a wrd b ; echo $wrd ; echod $a:$wrd:$b : ; }
         shift ;;
# if fractional, report remainder (for test purposes), keep wordlist (no shift)
 0.*|.*) : echo :REMAINDER: $idx ; echod :REM:$idx ;;
# any other input: search for word (case insensitive) following number,
# return index number or -1 if not found
 *) grep -i "^[ 	]*[0-9].*[	 ]$idx" $1 |
  { read a b ; echo ${a:--1}
  if test "$a" = ""
  then echo "error: '$idx' not found in list '$1'" >&2
  fi
  echod $idx:$a
  }
  shift ;;
 esac
 if test "$1" = ""
 then break
 fi
done
}

printhelp(){ cat <<EOH

usage: $0 [words|fractional number]
  will convert between words out of wordlists,
  and fractional number (pattern /[.0-9]*/) between 0 and 1
  wordlists: $wlists
   (may be set with NUMBERWORDS from the environment)
  allowing for $dcprec encoded digits or less

EOH
}

# convert word list into fractional number
tofraction(){
echod convert "'$@'" to fraction
# start dc script, setting fraction and seeding factor and polynome
echo "$dcprec2 k 0 1" >$dcsc
# convert words into index numbers
{ for wd in "$@"
do echo $wd
done } |
 pickwords $wlists |
{ for wl in $wllen
do read idx
# update product of wordlist lengths; add index and multiply by wordlist length
 echo "$wl*r$wl*$idx+r"
done } >>$dcsc
# divide polynome value by product, round to display precision, and print
echo "/ 10 _$dcprec ^2/+ $dcprec k1/p" >>$dcsc
dc -f $dcsc
}

# convert fractional number into word list
towords(){
echod "$1"
# now start dc script, storing fractional number in reg.e,
# pushing 0 and wordlist lengths, setting precision as precalculated
echo "$1 se 0 $wllen $dcprec2 k" >$dcsc
# invert sequence of lengths by pushing values onto reg.L
# until value 0, after again having started with 0
echo "0[SLd0<p]splpx" >>$dcsc
# print vector in base of wordlist values, one element per line
# ('0k1/' calculates integer value)
echo "[LL le*d 0k1/p $dcprec2 k-se lL 0<j]sj" >>$dcsc
# do it and append remainder from reg.e
echo "ljxlep" >>$dcsc

# run dc script, pick words from lists according to result
##dc $dcsc
##exit
dc $dcsc | pickwords $wlists | { while read wrd rem
# and concatenate them into a vector separated by spaces
 do case $wrd in
# print remainder to stderr, but ignore for wordlists
 :*) echod $wrd $rem ;;
 *) wds="$wds $wrd" ;;
 esac
done
echo $wds
}

} # towords

if test "$1" = ""
then printhelp
exit 9
fi

echod wlists $wlists
case $1 in
 [A-Za-z]*) tofraction "$@" ;;
# convert signs to dc syntax
 [0-9.]*) towords `echo "$1"|tr '+-' ' _'` ;;
# parameter reports for other calling scripts
# precision:
 :p) echo $dcprec ;;
# number of wordlists:
 :n) echo $wlcnt ;;
 *) printhelp ;;
esac

rm -f $dcsc
