#!/bin/sh
# inverse sort wordlists

{ while read word rem
# invert first word
 do iword=`echo $word | tac -r -s 'x\|[^x]'`
# and prepend to wordlist
 echo $iword $word $rem
 done
# then sort by inverted word, and remove it again from list
} | sort -k 1 | sed -e 's/[^ ]* //'
